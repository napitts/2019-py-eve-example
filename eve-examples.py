#!python3

# Short examples of python syntax
# With EVE flavor ...


class EveInstance:
    def __init__(self, server_name):
        self.name = server_name


def main():
    # Sample data
    ships = ['Atron,'
             'Condor,'
             'Kestrel,'
             'Merlin']

    ages = [25, 35, 38, 42, 48]

    planets = [['Amygnon', 1, 2, 3],
                'Jufvitte', [1, 2, 3, 4]]

    item_prices = {'Veldspar': 20.25,
                   'Arkonor': 37.63,
                   'Bistot': 15.6}

    ship_info = {'Atron': {
                     'Tank_Type': 'Buffer',
                     'Speed': 5,
                     'DPS': 3},
                 'Condor': {
                     'Tank_Type': 'Shield',
                     'Speed': 3,
                     'DPS': 2}}

    # Make class instance
    tranquility = EveInstance('Tranquility')

    tranquility.ships = ships
    tranquility.ages = ages
    tranquility.planets = planets
    tranquility.item_prices = item_prices
    tranquility.ship_info = ship_info

    # Sample List Syntax
    print("Python list")
    print("-"*70)
    print("Defined with brackets [ ]")
    print()

    print("Lists may contain items of the same type: ")
    print("print(tranquility.ages)")
    print(tranquility.ages)
    print()

    print("Lists may contain items of mixed type ... and may be nested")
    print("print(tranquility.planets)")
    print(tranquility.planets)
    print()

    print("Lists may be accessed via bracket indexing ... my_list[1]")
    print(f"tranquility.ages[1] = {tranquility.ages[1]}")
    print(f"tranquility.planets[0] = {tranquility.planets[0]}")
    print()

    print("Depending on the list ... you might get different types at different indices!")
    print(f"type(tranquility.planets[0][0]) = {type(tranquility.planets[0][0])}")
    print(f"tranquility.planets[0][0] = {tranquility.planets[0][0]}")
    print(f"type(tranquility.planets[0][1]) = {type(tranquility.planets[0][1])}")
    print(f"tranquility.planets[0][1] = {tranquility.planets[0][1]}")
    print()

    # Sample Dictionary Syntax
    print("Python dictionary")
    print("-" * 70)
    print("Defined with curly braces { }")
    print()

    print("Values may be same type: ")
    print("print(tranquility.item_prices)")
    print(tranquility.item_prices)
    print()

    print("Values may be mixed types ... and may be nested: ")
    print("print(tranquility.ship_info)")
    print(tranquility.ship_info)
    print()

    print("Values are indexed with any 'hashable' key")
    print(f"tranquility.item_prices['Veldspar'] = {tranquility.item_prices['Veldspar']}")
    print(f"tranquility.ship_info['Atron']['Speed'] = {tranquility.ship_info['Atron']['Speed']}")

    # Tuple
    # TBD

    # Set
    # TBD


if __name__ == '__main__':
    main()

